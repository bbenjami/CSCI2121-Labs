import java.util.Observable;

public class DiskSystemRunner{
    

public static void main(){

final long gigabyte = (long) Math.pow(10,9);

//Create a C: drive with 10 GB storage.
DiskDrive driveA = new DiskDrive("C:",10L * gigabyte);

//Create a D: drive with 20 GB storage.
DiskDrive driveB = new DiskDrive("D:",20L * gigabyte);

//Create a DiskDriveMonitor and register A and B with it.
DiskDriveMonitor monitor = new DiskDriveMonitor();

driveA.addObserver(monitor);
driveB.addObserver(monitor);

final long mebibyte = (long) Math.pow(2,20);

final long fileSize = (long)(512L * mebibyte);
int fileNumber = 1;
while (true){
try {
System.out.printf("Writing file %d to disk A...\n",fileNumber);
driveA.write(fileSize);

fileNumber++;

    
} catch (IllegalArgumentException e){
System.out.println("Could not write file to disk A.");
break;
//We couldn't write. End the loop.
}
}
}
}