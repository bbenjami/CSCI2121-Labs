import java.util.Observable;

public class DiskDrive extends Observable {


//The current usage, in bytes.
private long currentUsage;

//The maximum capacity, in bytes.
private final long maximumCapacity;

//Friendly label of the drive (e.g. "Disk 1" or "Backup Drive").
private String name;

//Constructs a new DiskDrive with a given maximum capacity.

public DiskDrive(final String name, final long maximumCapacity){
this.name = name;
this.maximumCapacity =
maximumCapacity;
this.currentUsage = 0;
}

//Writes a specified number of bytes to the drive.
public void write(final long bytes){
if (bytes <= 0){
throw new IllegalArgumentException("Must write a positive number of bytes.");

}

if (
this.currentUsage + bytes >this.maximumCapacity){
throw new IllegalArgumentException("Data will not fit.");
}

this.currentUsage += bytes;

this.setChanged();
this.notifyObservers();
}

//Returns the maximum capacity.
public long getMaximumCapacity(){
return this.maximumCapacity;
}

//Returns the current usage.
public long getCurrentUsage(){
return this.currentUsage;
}

//Returns the name of the disk drive.
public String getName(){
return this.name;
}
}