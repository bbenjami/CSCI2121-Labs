import java.util.Observable;
import java.util.Observer;

public class DiskDriveMonitor implements Observer{

//@Override

public void update(Observable obs, Object arg){
    
    DiskDrive drive = (DiskDrive) obs;
    
double fractionFull = ((float) drive.getCurrentUsage());

float max = ((float) drive.getMaximumCapacity());

if (fractionFull>= 0.85){

System.out.println("Disk Drive is 85% Full! Haven't you ever heard of defragging?");

}
}
}